
This package provides the minor mode `auto-compile-on-save-mode' which
automatically compiles Emacs Lisp code when the visiting buffers are
saved to their source files, provided that the respective byte code
files already exists.  If the byte code file does not already exist
nothing is done.  Also provided is `auto-compile-on-load-mode' which
is described toward the end of this commentary.

To start or stop compiling a source file or multiple files at once use
the command `toggle-auto-compile' which toggles automatic compilation
by either compiling the selected source file(s) or by removing the
respective byte code file(s).  The appropriate action is determined by
the existence respectively absence of the byte code file.

Automatically compiling Emacs Lisp source files after each save is
useful for at least the following reasons:

* Emacs prefers the byte code file over the source file even if the
  former is outdated.  Without a mode which automatically recompiles
  the source files you will at least occasionally forget to do so
  manually and end up with an old version of your code being loaded.

* There are many otherwise fine libraries to be found on the Internet
  which when compiled will confront the user with a wall of compile
  warnings and an occasional error.  If authors are informed about
  these (often trivial) problems after each save they will likely fix
  them quite quickly.  That or they have a high noise tolerance.

* It's often easier and less annoying to fix errors and warnings as
  they are introduced than to do a "let's compile today's work and see
  how it goes".

So do yourself and others a favor and enable this mode by adding the
following to your init file:

    (auto-compile-on-save-mode 1)

Auto-Compile mode is designed to stay out of your way as much as it
can while still motivating you to get things fixed.  But Auto-Compile
mode can also be configured to be more insistent, which might be
annoying initially but less so once existing problems have been fixed.

Occasionally you might be tempted to turn of Auto-Compile mode locally
because you are doing some work which causes lots of expected warnings
until you are actually done.  Don't do so: because Emacs prefers the
byte code file you would also have to remove that, in which case you
don't have to turn of this mode anymore.  In other words use the
command `toggle-auto-compile' instead.

Even when using `auto-compile-mode' it can sometimes happen that the
source file is newer than the byte compile destination.  This can for
example happen when performing version control operations. To ensure
that byte code files are always up-to-date when being loaded using
`require' and `load' enable `auto-compile-on-load-mode' which advises
this functions to recompile the source files when needed.  Enable this
mode before any potentially byte compiled files are loaded by beginning
your init file with:

    ;; -*- no-byte-compile: t -*-
    (add-to-list 'load-path "/path/to/auto-compile")
    (require 'auto-compile)
    (auto-compile-on-load-mode 1)
    (auto-compile-on-save-mode 1)

Also note that just because no warnings and/or errors are reported when
Auto-Compile mode compiles a source file this does not necessarily mean
that users of your libraries won't see any.  A likely cause for this
would be that you forgot to require a feature which is loaded on your
system but not necessarily on the users' systems.  So you should still
manually compile your packages before release:

    emacs -batch -Q -L . -L ../dependency/ -f batch-byte-compile *.el
