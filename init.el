;;;;;;;;;;;;;;;; load local elisps ;;;;;;;;;;;;;;;;
(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/")
(add-to-list 'load-path "~/.emacs.d/local-lisp/")

;;;;;;;;;;;;;;;;;;;;;; hooks ;;;;;;;;;;;;;;;;;;;;
(add-hook 'c-mode-hook
          (lambda ()
	    (define-key c-mode-map (kbd "C-c <C-return>") 'compile)))
(add-hook 'c++-mode-hook
          (lambda ()
	    (define-key c++-mode-map (kbd "C-c <C-return>") 'compile)))
;; (add-hook 'python-mode-hook
;;           (lambda ()
;;             (progn
;;             (flymake-python-pyflakes-load)
;;             (flymake-cursor-mode))))
;; (add-hook 'python-mode-hook 'elpy-mode)
;; (add-hook 'find-file-hook 'flymake-find-file-hook)

;;;;;;;;;;;;;;;;;; bindings ;;;;;;;;;;;;;;;;
;; (define-key global-map (kbd "C-<tab>") 'ac-trigger-key-command)
(global-unset-key (kbd "C-x C-b"))
(define-key global-map (kbd "C-x C-b") 'ibuffer)
(global-unset-key (kbd "C-\\"))
(define-key global-map (kbd "C-\\") 'set-mark-command)
(global-unset-key (kbd "C-x C-\\"))
(define-key global-map (kbd "C-x C-\\") 'pop-global-mark)
(global-unset-key (kbd "M-RET"))
(define-key global-map (kbd "M-RET") 'view-mode)
(global-unset-key (kbd "C-x C-b"))
(define-key global-map (kbd "s-b") 'ido-switch-buffer)
(global-unset-key (kbd "C-x C-b"))
(define-key global-map (kbd "C-x C-b") 'ibuffer)

;; (define-key python-mode-map (kbd "C"))
(global-set-key [(meta x)] (lambda ()
                             (interactive)
                             (or (boundp 'smex-cache)
                                 (smex-initialize))
                             (global-set-key [(meta x)] 'smex)
                             (smex)))

(global-set-key [(shift meta x)] (lambda ()
                                   (interactive)
                                   (or (boundp 'smex-cache)
                                       (smex-initialize))
                                   (global-set-key [(shift meta x)] 'smex-major-mode-commands)
                                   (smex-major-mode-commands)))



;;;;;;;;;;;;;;;;;;;; package ;;;;;;;;;;;;;;;;;
(require 'package-x)
(package-initialize)


;;;;;;;;;;;;;;;;;;;; Custom ;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ac-auto-show-menu nil)
 '(ac-auto-start t)
 '(ac-candidate-menu-min 2)
 '(ac-trigger-key "C-<tab>")
 '(ac-use-fuzzy t)
 '(ac-use-menu-map t)
 '(ansi-color-names-vector ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(auto-save-default t)
 '(autopair-global-mode t)
 '(backup-directory-alist (quote (("." . "backups"))))
 '(backup-inhibited t t)
 '(bookmark-default-file "~/.emacs.d/bookmark")
 '(company-idle-delay 0.5)
 '(cua-enable-cua-keys nil)
 '(current-language-environment "UTF-8")
 '(cursor-type (quote hollow))
 '(custom-safe-themes (quote ("4ddf47ae75239d8cf9d3d072eaf298066d0f6dbae6be3717c1210b08bdcb73d0" "68769179097d800e415631967544f8b2001dae07972939446e21438b1010748c" "8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "085b401decc10018d8ed2572f65c5ba96864486062c0a2391372223294f89460" "dad5950b1bd09d24fbc41cde5598c933da619fecc157c5e100809fa16cb5229e" "501caa208affa1145ccbb4b74b6cd66c3091e41c5bb66c677feda9def5eab19c" "72cc9ae08503b8e977801c6d6ec17043b55313cda34bcf0e6921f2f04cf2da56" "24377c1fcac8c5bf60b8df90dd43690f85c03223a27c65f50ca3560c863d59ac" "71efabb175ea1cf5c9768f10dad62bb2606f41d110152f4ace675325d28df8bd" "bf7ed640479049f1d74319ed004a9821072c1d9331bc1147e01d22748c18ebdf" "78b1c94c1298bbe80ae7f49286e720be25665dca4b89aea16c60dacccfbb0bca" "3580fb8e37ee9e0bcb60762b81260290329a97f3ca19249569d404fce422342f" "4aee8551b53a43a883cb0b7f3255d6859d766b6c5e14bcb01bed572fcbef4328" default)))
 '(dired-find-subdir t)
 '(dired-guess-shell-gnutar "gtar")
 '(dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\.")
 '(dired-x-hands-off-my-keys nil)
 '(echo-keystrokes 0.01)
 '(edit-server-verbose t)
 '(electric-indent-mode nil)
 '(electric-layout-mode t)
 '(electric-pair-mode nil)
 '(emacs-lisp-mode-hook (quote (#[nil "\300\301!\207" [checkdoc-minor-mode 1] 2] turn-on-eldoc-mode imenu-add-menubar-index)))
 '(exec-path (quote ("/Users/joe/.virtualenvs/speaking/bin" "/usr/local/bin" "/bin" "/usr/sbin" "/sbin" "/Applications/Emacs.app/Contents/MacOS/bin" "/usr/bin" "/opt/my/bin" "" nil)))
 '(fci-rule-color "#383838")
 '(fill-column 78)
 '(frame-title-format (quote ("%f - " user-real-login-name "@" system-name)) t)
 '(global-auto-complete-mode t)
 '(global-highlight-changes-mode nil)
 '(global-hl-line-mode t)
 '(global-linum-mode t)
 '(global-semantic-decoration-mode nil)
 '(global-semantic-highlight-edits-mode nil)
 '(global-semantic-highlight-func-mode nil)
 '(global-semantic-idle-summary-mode t)
 '(global-semantic-mru-bookmark-mode t)
 '(global-semantic-show-unmatched-syntax-mode t)
 '(global-semantic-stickyfunc-mode nil)
 '(highlight-symbol-idle-delay 0)
 '(ido-auto-merge-work-directories-length nil)
 '(ido-create-new-buffer (quote always))
 '(ido-enable-flex-matching t)
 '(ido-enable-prefix nil)
 '(ido-everywhere t)
 '(ido-ignore-extensions t)
 '(ido-max-prospects 8)
 '(ido-mode (quote both) nil (ido))
 '(ido-use-filename-at-point (quote guess))
 '(indent-tabs-mode nil)
 '(indicate-empty-lines t)
 '(inhibit-startup-screen t)
 '(jedi:setup-keys t)
 '(keyboard-coding-system (quote utf-8-unix))
 '(linum-format "  %d  ")
 '(lisp-interaction-mode-hook (quote (turn-on-eldoc-mode)))
 '(lisp-mode-hook (quote (imenu-add-menubar-index slime-lisp-mode-hook)))
 '(mf-offset-x 54)
 '(mouse-wheel-scroll-amount (quote (1 ((shift) . 1) ((control)))))
 '(package-archives (quote (("gnu" . "http://elpa.gnu.org/packages/") ("marmalade" . "http://marmalade-repo.org/packages/") ("melpa" . "http://melpa.milkbox.net/packages/"))))
 '(package-enable-at-startup t)
 '(package-load-list (quote (all auto-complete)))
 '(puppet-indent-level tab-width)
 '(py-shell-name "ipython")
 '(py-start-run-ipython-shell t)
 '(pylint-command "/usr/local/bin/pylint")
 '(python-indent-guess-indent-offset t)
 '(python-indent-offset 4)
 '(python-skeleton-autoinsert t)
 '(recentf-max-saved-items 75)
 '(require-final-newline t)
 '(ruby-indent-level tab-width)
 '(scroll-preserve-screen-position t)
 '(semantic-default-submodes (quote (global-semantic-highlight-func-mode global-semantic-decoration-mode global-semantic-stickyfunc-mode global-semantic-idle-completions-mode global-semantic-idle-scheduler-mode global-semanticdb-minor-mode global-semantic-idle-summary-mode global-semantic-mru-bookmark-mode)))
 '(semantic-mode t)
 '(server-host nil)
 '(server-mode t)
 '(server-use-tcp t)
 '(set-mark-command-repeat-pop t)
 '(show-paren-delay 0)
 '(show-paren-mode t)
 '(slime-net-coding-system (quote utf-8-unix))
 '(tab-width 4)
 '(tabbar-mode t nil (tabbar))
 '(tabbar-separator (quote (1)))
 '(tool-bar-mode nil)
 '(vc-make-backup-files t)
 '(xterm-mouse-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cursor ((t (:background "white" :foreground "#F92672"))))
 '(highlight-symbol-face ((t (:background "midnight blue"))))
 '(highline-face ((t (:background "windowBackgroundColor"))) t)
 '(highline-vertical-face ((t (:background "windowBackgroundColor"))) t)
 '(hl-line ((t (:background "gray9"))))
 '(show-paren-match ((t (:background "DodgerBlue4"))))
 '(tabbar-button ((t (:inherit tabbar-default :box (:line-width 1 :color "gray15" :style released-button)))))
 '(tabbar-default ((t (:inherit variable-pitch :background "gray20" :foreground "grey40" :height 1))))
 '(tabbar-selected ((t (:inherit tabbar-default :background "gray12" :foreground "gray80" :box (:line-width 1 :color "gray10" :style released-button) :weight bold))))
 '(tabbar-separator ((t (:inherit tabbar-default :height 1))))
 '(tabbar-unselected ((t (:inherit tabbar-default :box (:line-width 1 :color "gray10" :style pressed-button))))))


;;;;;;;;;;;;;;;; dired-x ;;;;;;;;;;;;;;;;
(require 'dired-x)
;;;;;;;;;;;;;;;;;;;; fold-dwim ;;;;;;;;;;;;;;;;;;;;
(require 'fold-dwim)
;;;;;;;;;;;;;;;; jedi ;;;;;;;;;;;;;;;;
;; (require 'jedi)
;; (jedi:setup)
;; (autoload 'jedi:setup "jedi" nil t)
;; (add-hook 'python-mode-hook 'jedi:setup)
;; (add-hook 'python-mode-hook 'jedi:ac-setup)

;;;;;;;;;;;;;;;;;;;; auto-complete ;;;;;;;;;;;;;;;;;;;;
(require 'auto-complete)
;; (global-auto-complete-mode t)
;;;;;;;;;;;;;;;;;;;; highlight-symbol ;;;;;;;;;;;;;;;;;;;;
(require 'highlight-symbol)
(define-globalized-minor-mode global-highlight-symbol-mode highlight-symbol-mode (lambda ()(highlight-symbol-mode t)))
(global-highlight-symbol-mode t)
;;;;;;;;;;;;;;;;;;;; autopair ;;;;;;;;;;;;;;;;
;; (autopair-global-mode t)

;;;;;;;;;;;;;;;; comment current line ;;;;;;;;;;;;;;;;;;;;
(require 'comment-dwim-line)
(global-set-key "\M-;" 'comment-dwim-line)


;;;;;;;;;;;;;;;;;;;; Tramp ;;;;;;;;;;;;;;;;;;;;
(require 'tramp)
(add-to-list 'tramp-default-proxies-alist '("nlp-s1" nil "/ssh:nlp-service1.cs.nthu.edu.tw#2222:"))

(add-to-list 'tramp-default-proxies-alist
	     '(nil  "\\`root\\'" "/ssh:%h:"))
(add-to-list 'tramp-default-proxies-alist
	     '((regexp-quote (system-name)) nil nil))

(load-theme 'monokai t )    ;; d22
(if (window-system)
    (progn
      (menu-bar-mode t)
     ;; (load-theme 'monokai t)
      )
  (progn
    (menu-bar-mode nil)
   ;; (load-theme 'wombat t))
    ))
;;;;;;;;;;;;;;;; flycheck, flymake ;;;;;;;;;;;;;;;;

;; (require 'flymake-cursor)
;; (when (load "flymake" t)
;;   (defun flymake-pyflakes-init ()
;;     (let* ((temp-file (flymake-init-create-temp-buffer-copy
;;                        'flymake-create-temp-inplace))
;;            (local-file (file-relative-name
;;                         temp-file
;;                         (file-name-directory buffer-file-name))))
;;       (list "pyflakes" (list local-file))))

;;   (add-to-list 'flymake-allowed-file-name-masks
;;                '("\\.py\\'" flymake-pyflakes-init)))

;;;;;;;;;;;;;;;;
(require 'repeatable)
(define-key global-map (kbd "C-x g") (repeatable-command-def 'tabbar-forward-group))
(define-key global-map (kbd "C-x t") (repeatable-command-def 'tabbar-forward-tab))

;;;;;;;;;;;;;;;; ropemacs ;;;;;;;;;;;;;;;;;;;;

;; (require 'pymacs)
;; (autoload 'pymacs-apply "pymacs")
;; (autoload 'pymacs-call "pymacs")
;; (autoload 'pymacs-eval "pymacs" nil t)
;; (autoload 'pymacs-exec "pymacs" nil t)
;; (autoload 'pymacs-load "pymacs" nil t)
;; (pymacs-load "ropemacs" "rope-")

  ;; (pymacs-load "ropemacs" )
;; (setq ropemacs-enable-autoimport t)

;; Diminish modeline clutter
;; (require 'diminish)
;; (diminish 'wrap-region-mode)
;; (diminish 'yas/minor-mode)
(setenv "PATH" "/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin")
;; (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
;; (setq ipython-command "/usr/bin/ipython")
;; (setq py-python-command "/usr/bin/ipython")
;; (require 'ipython)
;; (setq py-python-command-args '("-pylab" "-colors" "DarkBG"))

;; (setq
;;  python-shell-interpreter "ipython"
;;  python-shell-interpreter-args ""
;;  python-shell-prompt-regexp "In \\[[0-9]+\\]: "
;;  python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
;;  python-shell-completion-setup-code
;;    "from IPython.core.completerlib import module_completion"
;;  python-shell-completion-module-string-code
;;    "';'.join(module_completion('''%s'''))\n"
;;  python-shell-completion-string-code
;;    "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")

(add-to-list 'load-path "~/.emacs.d/local-lisp/python-mode.el-6.1.1")
(setq py-install-directory "~/.emacs.d/local-lisp/python-mode.el-6.1.1")
(require 'python-mode)

;; (setq inferior-lisp-program "/usr/local/bin/sbcl")
(setq slime-lisp-implementations
      '((sbcl ("sbcl"))
        (ccl ("C:/home/bin/ccl/wx86cl.exe"))
        (clisp ("c:/home/bin/clisp/full/lisp.exe" "-B" "c:/home/bin/clisp/full" "-M" "c:/home/bin/clisp/full/lispinit.mem" "-ansi" "-q"))
        ))

(require 'slime-autoloads)
;; (slime-setup '(slime-scratch slime-editing-commands))
(add-to-list 'load-path "/Users/joe/.emacs.d/elpa/slime-20130402.1441")
(add-to-list 'load-path "/Users/joe/.emacs.d/elpa/slime-20130402.1441/contrib")
(add-hook 'slime-load-hook (lambda () (require 'slime-fuzzy)
                             (slime-fuzzy-init)))

(slime-setup '(slime-fancy slime-asdf slime-tramp))
